//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/	 /*Update Interval*/	/*Update Signal*/

  {" 📦 ", "/home/raul/.local/bin/pacupdate_dt",	   120,		              0},
  {"| ", "/home/raul/.local/bin/weather",              300,                   0},
  {"| 🖥 ", "/home/raul/.local/bin/cpu3",              6,                     0},
  /* {"🌡", "/home/raul/.local/bin/cpu",               1,                     0}, */
  {"| 🧠 ", "/home/raul/.local/bin/memory_dt",	       6,                     0},
  {"| 🗄 ", "/home/raul/.local/bin/disk",              300,                   0},
  {"| ", "/home/raul/.local/bin/battery",             60,                    0},
  /* {" | ", "/home/raul/.local/bin/internet",         10,                    0}, */
  /* {" | ", "/home/raul/.local/bin/vol",			   2,                     0}, */
  {"| 🕑 ", "/home/raul/.local/bin/clock_dt",	       10,                    0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim = ' ';
